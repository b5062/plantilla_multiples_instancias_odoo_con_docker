### Arquitectura 3

Arquitectura para múltiples instancias de Odoo conectados a un servidor de base de datos externo.
Nota: El servidor de base de datos y al de aplicaciones (VPS con Odoo’s) debe encontrarse en el mismo servidor.


#### Consideraciones
1. El servidor de base de datos externo debe encontrarse en la misma red que el servidor de aplicaciones, de otro modo la interacción entre ambos será demasiada lenta.
