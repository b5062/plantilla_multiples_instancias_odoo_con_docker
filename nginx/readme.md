# Configuración de dominio con Nginx

## Instalación de nginx
sudo apt install nginx

## Instalación de certbot para uso de SSL de let's encrypt
Detalle aquí https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/
Pasos de instalación:
~~~
apt-get update
sudo apt-get install certbot
sudo apt-get install python3-certbot-nginx
~~~

## Archivo de configuración de dominio en nginx
El archivo e0.bigodoo.net.conf es un ejemplo de como se debería configurar un dominio en nginx.
Para este ejemplo:
* El nombre del dominio es e0.bigodoo.net (la extensión .conf es para que nginx pueda leer el archivo)

El archivo debe localizarse dentro de la carpeta /etc/nginx/conf.d
Para que este cambio surta efecto debe reiniciar el servicio con el comando
~~~
sudo service nginx reload
~~~
Debe asegurarse que el dominio este asociado con la IP. Si esto último esta verificado, entonces puede ir al navegador e ingresar el dominio y debería dirigirlo a la interfaz de autenticación o de creación de base de datos de Odoo. 

## Obteniendo el certificado con certbot
Ubicarse en la raíz del directorio /etc/nginx/conf.d

Ejecutar los siguientes comandos:
~~~
sudo certbot --nginx -d e0.bigodoo.net
*Seleccinar opción 2*
sudo service nginx reload
~~~
